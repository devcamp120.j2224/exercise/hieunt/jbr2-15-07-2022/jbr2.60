public class App {
    public static void main(String[] args) throws Exception {
        Animal animal1 = new Animal("Fish");
        Animal animal2 = new Animal("Bird");
        System.out.println("Animal1 la: " + animal1);
        System.out.println("Animal2 la: " + animal2);
        System.out.println("");
        Mammal mammal1 = new Mammal("Cow");
        Mammal mammal2 = new Mammal("Buffalo");
        System.out.println("Mammal1 la: " + mammal1);
        System.out.println("Mammal2 la: " + mammal2);
        System.out.println("");
        Cat cat1 = new Cat("Nikki");
        Cat cat2 = new Cat("Mimi");
        System.out.println("Cat1 la: " + cat1);
        System.out.println("Cat2 la: " + cat2);
        System.out.println("");
        Dog dog1 = new Dog("Milu");
        Dog dog2 = new Dog("Husky");
        System.out.println("Dog1 la: " + dog1);
        System.out.println("Dog2 la: " + dog2);
        System.out.println("");
        System.out.print("Cat1 keu: ");
        cat1.greets();
        System.out.print("Cat2 keu: ");
        cat2.greets();
        System.out.println("");
        System.out.print("Dog1 keu: ");
        dog1.greets();
        System.out.print("Dog2 keu: ");
        dog2.greets();
        System.out.println("");
        System.out.print("Dog1 keu voi dog2: ");
        dog1.greets(dog2);
    }
}
